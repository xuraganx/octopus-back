const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Card = require('../models/card');

const leadsBoardSchema = new Schema({
  userId: String,
  lanes: [{
    id: String,
    title: String,
    cards: [Card.schema],
  }],
});

module.exports = mongoose.model('LeadsBoard', leadsBoardSchema);
