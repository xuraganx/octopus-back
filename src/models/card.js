const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cardSchema = new Schema({
  id: String,
  threadId: String,
  user: Object,
  comments: [Object],
  firstMessage: Object,
  lastMessage: Object,
  title: String,
  note: String,
});

module.exports = mongoose.model('CardBoard', cardSchema);
