const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const sessionSchema = new Schema({
  pk: Number,
  cookies: String,
  state: {
    deviceString: String,
    deviceId: String,
    uuid: String,
    phoneId: String,
    adid: String,
    build: String,
  },
});

module.exports = mongoose.model('Session', sessionSchema);
