const express = require('express');
const router = express.Router();

const utils = require('../utils');

router.get('/', async (req, res) => {
  try {
    const { ig } = await utils.initApiClient(req.headers.token);
    const inboxFeed = ig.feed.directInbox();
    const threads = await inboxFeed.items();
    const data = {
      threads,
    };
    res.json(data);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.post('/', async (req, res) => {
  try {
    const { ig } = await utils.initApiClient(req.headers.token);
    const { message, userId } = req.body;
    const thread = ig.entity.directThread([userId.toString()]);
    await thread.broadcastText(message);
    res.json({});
  } catch (error) {
    res.status(400).send(error);
  }
});

// https://github.com/dilame/instagram-private-api/issues/810
// router.get('/chatRequests', async (req, res) => {
//   try {
//     const ig = await utils.initApiClient(req.headers.token);
//     const directPending = ig.feed.directPending();
//     const requests = await directPending.items();
//     res.json({ requests });
//   } catch (error) {
//     console.log('error', error);
//     res.json({ error });
//   }
// });

router.get('/feed', async (req, res) => {
  try {
    const { ig, pk } = await utils.initApiClient(req.headers.token);
    const userFeed = ig.feed.user(pk);
    const feed = await userFeed.items();
    res.json({ feed });
  } catch (error) {
    console.log('get /feed error', error);
    res.status(400).send(error);
  }
});

router.get('/feed/comments', async (req, res) => {
  try {
    const { ig } = await utils.initApiClient(req.headers.token);
    const mediaComments = ig.feed.mediaComments(req.query.mediaId);
    const comments = await mediaComments.items();
    res.json({ comments });
  } catch (error) {
    console.log('get /feed/comments error', error);
    res.status(400).send(error);
  }
});

module.exports = router;
