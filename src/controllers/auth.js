const express = require('express');
const router = express.Router();
const IgApiClient = require('instagram-private-api/dist/index').IgApiClient;

const Session = require('../models/session');

router.post('/login', async (req, res) => {
  try {
    const IG_USERNAME = req.body.username;
    const IG_PASSWORD = req.body.password;
    const ig = new IgApiClient();
    ig.state.generateDevice(IG_USERNAME);
    await ig.simulate.preLoginFlow();
    const loggedInUser = await ig.account.login(IG_USERNAME, IG_PASSWORD);
    process.nextTick(async () => await ig.simulate.postLoginFlow());
    const cookies = await ig.state.serializeCookieJar();
    const state = {
      deviceString: ig.state.deviceString,
      deviceId: ig.state.deviceId,
      uuid: ig.state.uuid,
      phoneId: ig.state.phoneId,
      adid: ig.state.adid,
      build: ig.state.build,
    };

    let session = await Session.findOneAndUpdate({ pk: loggedInUser.pk }, {
      cookies: JSON.stringify(cookies),
      state,
    });

    if (!session) {
      session = await Session.create({
        pk: loggedInUser.pk,
        cookies: JSON.stringify(cookies),
        state,
      });
    }

    res.json({ user: loggedInUser, token: session._id });
  } catch (error) {
    res.status(400).send(error);
  }
});

module.exports = router;
