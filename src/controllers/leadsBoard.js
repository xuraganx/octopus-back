const express = require('express');
const router = express.Router();

const LeadsBoard = require('../models/leadsBoard');
const Card = require('../models/card');
const utils = require('../utils');

const defaultLeadBoard = {
  lanes: [
    {
      id: 'lane0',
      title: 'Необработанные',
      cards: [],
    },
    {
      id: 'lane1',
      title: 'Интерес, диалог',
      cards: [],
    },
    {
      id: 'lane2',
      title: 'Думает',
      cards: [],
    },
    {
      id: 'lane3',
      title: 'Купил',
      cards: [],
    },
    {
      id: 'lane4',
      title: 'Сбор NPS',
      cards: [],
    },
  ],
};

router.get('/', async (req, res) => {
  try {
    const token = req.headers.token;
    let leadsBoard = await LeadsBoard.findOne({ userId: token });
    if (!leadsBoard) {
      defaultLeadBoard.userId = token;
      leadsBoard = await LeadsBoard.create(defaultLeadBoard);
    }

    const { ig, pk } = await utils.initApiClient(req.headers.token);
    const userFeed = ig.feed.user(pk);
    const feed = await userFeed.items();
    const comments = await getComments(ig, pk, feed);
    const inboxFeed = ig.feed.directInbox();
    const threads = await inboxFeed.items();
    const messages = getMessages(threads);
    const cards = getCards(comments, messages);

    const existsCards = [];
    leadsBoard && leadsBoard.lanes && leadsBoard.lanes.length && leadsBoard.lanes.forEach((lane) => {
      Array.prototype.push.apply(existsCards, lane.cards);
    });

    existsCards.forEach((existsCard) => {

      const updatedCard = cards[existsCard.user.pk];
      if (updatedCard) {
        existsCard.threadId = updatedCard.threadId;
        existsCard.comments = updatedCard.comments;
        existsCard.firstMessage = updatedCard.firstMessage;
        existsCard.lastMessage = updatedCard.lastMessage;
        delete cards[existsCard.user.pk]
      }
    });

    for (let key in cards) {
      leadsBoard.lanes[0].cards.push(cards[key]);
    }

    leadsBoard.lanes.forEach((lane) => {
      lane.cards.forEach((card) => {
        card.id = card._id;
      });
    });

    await LeadsBoard.findByIdAndUpdate({ _id: leadsBoard._id }, leadsBoard);

    res.json({ leadsBoard });
  } catch (error) {
    res.status(400).send(error);
  }
});

router.put('/', async (req, res) => {
  try {
    const token = req.headers.token;
    const { cardId, sourceLaneId, targetLaneId, position, cardDetails } = req.body;
    const leadsBoard = await LeadsBoard.findOne({ userId: token });
    if (!leadsBoard) {
      throw new Error('Can not find leadsBoard');
    }
    const sourceLaneIndex = leadsBoard.lanes.findIndex((lane) => (lane.id === sourceLaneId));
    const targetLaneIndex = leadsBoard.lanes.findIndex((lane) => (lane.id === targetLaneId));
    const sourceCardIndex = leadsBoard.lanes[sourceLaneIndex].cards.findIndex((card) => (card.id === cardId));
    leadsBoard.lanes[sourceLaneIndex].cards.splice(sourceCardIndex, 1);
    leadsBoard.lanes[targetLaneIndex].cards.splice(position, 0, cardDetails);
    await LeadsBoard.findByIdAndUpdate({ _id: leadsBoard._id }, leadsBoard);
    res.json({ leadsBoard });
  } catch (error) {
    res.status(400).send(error);
  }
});

router.get('/leads/:id', async (req, res) => {
  try {
    const token = req.headers.token;
    const leadsBoard = await LeadsBoard.findOne({ 'lanes.cards._id': req.params.id }, { 'lanes.cards.$': 1 });
    const lead = leadsBoard ? leadsBoard.lanes[0].cards.find((card) => (card._id.toString() === req.params.id)) : null;
    res.json({ lead, status: !!lead ? leadsBoard.lanes[0].title : null });
  } catch (error) {
    console.log('err', error);
    res.status(400).send(error);
  }
});


async function getComments(ig, pk, feed) {
  const comments = [];
  if (feed && feed.length) {
    for (let i = 0; i < feed.length; i++) {
      const mediaComments = ig.feed.mediaComments(feed[i].caption.media_id);
      const postComments = await mediaComments.items();
      Array.prototype.push.apply(comments, postComments);
    }
  }

  const usersComments = {};
  comments.forEach((comment) => {
    if (comment.user_id !== pk) {
      if (usersComments[comment.user_id]) {
        usersComments[comment.user_id].push(comment);
      } else {
        usersComments[comment.user_id] = [comment];
      }
    }
  });
  return usersComments;
}

function getMessages(threads) {
  const userMessages = {};
  threads.forEach((thread) => {
    const user = thread.users[0];
    const firstMessage = thread.items.find((item) => (item.user_id === user.pk));
    const lastMessage = thread.items.reverse().find((item) => (item.user_id === user.pk));
    userMessages[user.pk] = {
      lastMessage,
      firstMessage,
      threadId: thread.thread_id,
      user,
    };
  });
  return userMessages;
}

function getCards(comments = {}, messages = {}) {
  const cards = {};
  const commentsKeys = Object.keys(comments);
  for (let i = 0; i < commentsKeys.length; i++) {
    if (!cards[commentsKeys[i]]) {
      cards[commentsKeys[i]] = {};
    }
    cards[commentsKeys[i]].comments = comments[commentsKeys[i]];
    cards[commentsKeys[i]].user = comments[commentsKeys[i]][0].user;
  }

  const messagesKeys = Object.keys(messages);
  for (let i = 0; i < messagesKeys.length; i++) {
    if (!cards[messagesKeys[i]]) {
      cards[messagesKeys[i]] = {};
    }
    cards[messagesKeys[i]].threadId = messages[messagesKeys[i]].threadId;
    cards[messagesKeys[i]].user = messages[messagesKeys[i]].user;
    cards[messagesKeys[i]].firstMessage = messages[messagesKeys[i]].firstMessage;
    cards[messagesKeys[i]].lastMessage = messages[messagesKeys[i]].lastMessage;
  }

  const cardsKeys = Object.keys(cards);
  for (let i = 0; i < cardsKeys.length; i++) {
    const titleCandidates = [];
    if (cards[cardsKeys[i]].firstMessage) {
      titleCandidates.push({
        timestamp: parseInt(cards[cardsKeys[i]].firstMessage.timestamp) / 1000,
        title: cards[cardsKeys[i]].firstMessage.text,
      });
    }
    if (cards[cardsKeys[i]].comments) {
      cards[cardsKeys[i]].comments.forEach((comment) => {
        titleCandidates.push({
          timestamp: comment.created_at * 1000,
          title: comment.text,
        });
      })
    }
    const sortedCandidates = titleCandidates.sort((a, b) => {
      if (a.timestamp < b.timestamp) {
        return -1;
      }
      if (a.timestamp > b.timestamp) {
        return 1;
      }
      return 0;
    });

    cards[cardsKeys[i]].title = sortedCandidates[0].title;
    cards[cardsKeys[i]].note = '';
  }
  return cards;
}

module.exports = router;
