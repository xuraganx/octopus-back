const IgApiClient = require('instagram-private-api/dist/index').IgApiClient;

const Session = require('../models/session');

async function initApiClient(token) {
  const session = await Session.findOne({ _id: token });
  if (session) {
    const { state, cookies, pk } = session;
    const ig = new IgApiClient();
    await ig.state.deserializeCookieJar(cookies);
    ig.state.deviceString = state.deviceString;
    ig.state.deviceId = state.deviceId;
    ig.state.uuid = state.uuid;
    ig.state.phoneId = state.phoneId;
    ig.state.adid = state.adid;
    ig.state.build = state.build;

    return { ig, pk };
  } else {
    throw new Error('Can not find session');
  }
}


module.exports = {
  initApiClient,
};
