const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');

const app = express();

const igController = require('./src/controllers/ig');
const authController = require('./src/controllers/auth');
const leadsBoardController = require('./src/controllers/leadsBoard');

mongoose.connect('mongodb://octopus:UGvhMJWY4PSWWuU@ds347467.mlab.com:47467/octopus',  {
    useNewUrlParser: true,
    useFindAndModify: false,
}, (err) => {
    if (err) {
        console.log('connection error', err);
    } else {
        console.log('connection successful');
    }
});

const API_URL = '/api/v1';

app.use(bodyParser.json());
app.use(cors({origin: '*'}));
app.use(`${API_URL}/ig`, igController);
app.use(`${API_URL}/auth`, authController);
app.use(`${API_URL}/leadsBoard`, leadsBoardController);

app.use((req, res, next) => {
    res.status(404);
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use((err, req, res) => {
    res.status(err.status || 500);
    console.log('error', err);
    res.json({
        message: err.message,
        error: {}
    });
});

app.set('port', process.env.PORT || 3000);

const server = app.listen(app.get('port'), () => {
    console.log('\n--------App runned on port: ' + server.address().port + '----------\n');
});
